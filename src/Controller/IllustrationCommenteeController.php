<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\IllustrationCommentee;
use App\Entity\Category;
use App\Repository\IllustrationCommenteeRepository;
use App\Repository\CategoryRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
* @Route("/api", name="api")
*/
class IllustrationCommenteeController extends AbstractController
{
    private $serializer;
    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
    * @Route("/", methods="GET")
    */
    public function getAll(categoryRepository $repo)
    {
        $data = $repo->findAll();
        $jsonContent = $this->serializer->serialize($data, "json");
        return JsonResponse::fromJsonString($jsonContent);
    }

    /**
    * @Route("/admin/add", methods="POST")
    */
    public function add(Request $request, ObjectManager $manager)
    {
        $content = json_decode($request->getContent(), true);
        $entityAdd = new IllustrationCommentee();
        $entityAdd->setBase64Image($content["image"]);
        $entityAdd->setDescription($content["description"]);
        $entityAdd->setTemoignage($content["temoignage"]);
        $entityAdd->setCategory($content["category"]);
        $manager->persist($entityAdd);
        $manager->flush();
        $jsonContent = $this->serializer->serialize($entityAdd->getId(), "json");
        return JsonResponse::fromJsonString($jsonContent);
    }

    /**
    * @Route("/category/add", methods="POST")
    */
    public function addCategory(Request $request, ObjectManager $manager)
    {
        $content = json_decode($request->getContent(), true);
        $entityAdd = new Category();
        $entityAdd->setBase64Image($content["image"]);
        $entityAdd->setName($content["name"]);
        $manager->persist($entityAdd);
        $manager->flush();
        $jsonContent = $this->serializer->serialize($entityAdd->getId(), "json");
        return JsonResponse::fromJsonString($jsonContent);
    }

    /**
    * @Route("/admin/{id}", methods="PUT")
    */
    public function edit(IllustrationCommentee $entity, ObjectManager $manager, Request $request) {
        $content = json_decode($request->getContent(), true);
        $entity->setBase64Image($content["image"]);
        $entity->setDescription($content["description"]);
        $entity->setTemoignage($content["temoignage"]);
        $entity->setCategory($content["category"]);
        $manager->persist($entity);
        $manager->flush();
    }

    /**
    * @Route("/admin/category/{id}", methods="PUT")
    */
    public function editCategory(Category $entity, ObjectManager $manager, Request $request) {
        $content = json_decode($request->getContent(), true);
        $entity->setBase64Image($content["image"]);
        $entity->setName($content["Name"]);
        $manager->persist($entity);
        $manager->flush();
    }

    /**
    * @Route("/admin/remove/{id}", methods="DELETE")
    */
    public function remove(IllustrationCommentee $entity, ObjectManager $manager) {
    $manager->remove($entity);
    $manager->flush();
    }

    /**
    * @Route("/admin/remove/{id}", methods="DELETE")
    */
    public function removeCategory(Category $entity, ObjectManager $manager) {
        $manager->remove($entity);
        $manager->flush();
    }
}